#include <sys/poll.h>
#include <bits/poll.h>
#include <sys/socket.h>
#include <iostream>
#include <algorithm>
#include <csignal>
#include <fcntl.h>
#include <cstring>
#include "ThreadWorker.h"
#include "Config.h"
#include "helpers/HttpParser.h"
#include "helpers/HostConnector.h"

ThreadWorker::ThreadWorker(Storage &newStorage) : storage(newStorage) {
    thread = std::thread([this]() { ThreadWorker::worker(); });
    if (pipe2(addPipeFd, O_NONBLOCK) == -1 || pipe2(removePipeFd, O_NONBLOCK) == -1) {
        throw std::runtime_error("Error creating pipes");
    }

    fds.push_back({addPipeFd[0], POLLIN, 0});
    fds.push_back({removePipeFd[0], POLLIN, 0});
}

void ThreadWorker::worker() {
    while (true) {
        int pollResult = poll(fds.data(), fds.size(), -1);
        //printf("poll() %d\n", pollResult);
        // for(size_t i = 0; i < fds.size(); i++){
        //     if(fds[i].revents != 0){
        //         printf("FD %d, %ld\n", fds[i].fd, serverSocketsURI.count(fds[i].fd));
        //     }
        // }
        if (pollResult == -1) {
            throw std::runtime_error("poll error");
        }

        handlePipeMessages();
        applyPendingChanges();

        for (size_t i = 2; i < fds.size(); i++) {
            auto &pfd = fds[i];
            if (serverSocketsURI.count(pfd.fd)) {
                handleReadDataFromServer(pfd);
                continue;
            }

            handleClientConnection(pfd);
        }

    }
}

void ThreadWorker::storeClientConnection(int fd) {
    struct pollfd pfd{};
    pfd.fd = fd;
    pfd.events = POLLIN;
    addPipe(pfd.fd);
}

void ThreadWorker::handleClientConnection(pollfd &pfd) {
    if (pfd.revents & POLLIN) {
        handleClientInput(pfd);
        return;
    }

    if (pfd.revents & POLLOUT) {
        handleClientReceivingResource(pfd);
        return;
    }
}

void ThreadWorker::handleClientInput(pollfd &pfd) {
    //printf("ASD\n");
    readClientInput(pfd.fd);

    auto &clientBuf = clientBuffersMap[pfd.fd];
    if (!HttpParser::isHttpRequestComplete(clientBuf)) {
        return;
    }

    auto req = HttpParser::parseRequest(clientBuf);

    clientSocketsURI.insert(std::make_pair(pfd.fd, req.uri));

    if (!storage.containsKey(req.uri)) {
        storage.initElement(req.uri);
        auto serverFD = HostConnector::connectToTargetHost(req);
        printf("add server socket %d\n", serverFD);
        addPipe(serverFD);
        serverSocketsURI.insert(std::make_pair(serverFD, req.uri));
    }


    pfd.events = POLLOUT;
    auto *cacheElement = storage.getElement(req.uri);
    cacheElement->initReader(pfd.fd);
    clientBuf.clear();
}

void ThreadWorker::readClientInput(int fd) {
    char buf[CHUNK_SIZE];
    ssize_t bytesRead = recv(fd, buf, CHUNK_SIZE, 0);

    if (bytesRead < 0) {
        //throw std::runtime_error("recv input from data");
    }

    auto &clientBuf = clientBuffersMap[fd];

    clientBuf += std::string(buf, bytesRead);
}

void ThreadWorker::handleClientReceivingResource(pollfd &pfd) {
    auto uri = clientSocketsURI.at(pfd.fd);
    auto *cacheElement = storage.getElement(uri);

    auto data = cacheElement->readData(pfd.fd);
    if (!data.empty()) {
        ssize_t bytesSend = send(pfd.fd, data.data(), data.size(), 0);
        if (bytesSend == -1 && errno == EPIPE) {
            removePipe(pfd.fd);
        }
    }

    if (cacheElement->isFinishReading(pfd.fd)) {
        removePipe(pfd.fd);
        return;
    }

    if(data.empty()){
        pfd.events &= ~POLLOUT;
    }
}

void ThreadWorker::handleReadDataFromServer(pollfd &pfd) {
    auto uri = serverSocketsURI.at(pfd.fd);
    auto *cacheElement = storage.getElement(uri);
    assert(cacheElement != nullptr);
    auto &bufToReceiveStatusCode = clientBuffersMap.at(pfd.fd);

    if (cacheElement->isFinished()) {
        auto code = HttpParser::parseStatusCode(bufToReceiveStatusCode);

        if (code < 0) {
            return;
        }

        size_t readersCount = cacheElement->getReadersCount();
        if (code == 200 && readersCount == 0) {
            removePipe(pfd.fd);
            storage.clearElement(uri);
        }

        printf("Status code %d\n", code);
        bufToReceiveStatusCode.clear();
        return;
    }

    char buf[CHUNK_SIZE] = {'\0'};
    ssize_t bytesRead = recv(pfd.fd, buf, CHUNK_SIZE, 0); // == -1??
    cacheElement->appendData(std::string(buf, bytesRead));

    if (!HttpParser::isStatusCodeReceived(bufToReceiveStatusCode)) {
        bufToReceiveStatusCode += std::string(buf, bytesRead);
    }

    if (bytesRead == 0) {
        cacheElement->markFinished();
        return;
    }

    //printf("Bytes read %zd\n", bytesRead);
}

void ThreadWorker::addPipe(int writeEnd) {
    if (write(addPipeFd[1], &writeEnd, sizeof(writeEnd)) == -1) {
        throw std::runtime_error("Error writing to addPipe");
    }
}

void ThreadWorker::removePipe(int writeEnd) {
    if (write(removePipeFd[1], &writeEnd, sizeof(writeEnd)) == -1) {
        throw std::runtime_error("Error writing to removePipe");
    }
}

void ThreadWorker::handlePipeMessages() {
    int readEnd;

    while (read(addPipeFd[0], &readEnd, sizeof(readEnd)) != -1) {
        pendingChanges.emplace_back(readEnd, PipeOperation::Add);
    }

    while (read(removePipeFd[0], &readEnd, sizeof(readEnd)) != -1) {
        pendingChanges.emplace_back(readEnd, PipeOperation::Remove);
    }
}

void ThreadWorker::applyPendingChanges() {
    for (const auto &change: pendingChanges) {
        int fd = change.first;
        PipeOperation operation = change.second;

        if (operation == PipeOperation::Add) {
            struct pollfd pfd{};
            pfd.fd = fd;
            pfd.events = POLLIN;
            fds.emplace_back(pfd);
            clientBuffersMap.insert(std::make_pair(pfd.fd, std::string()));
        } else if (operation == PipeOperation::Remove) {
            printf("Erase %d\n", fd);
            fds.erase(std::remove_if(fds.begin(), fds.end(),
                                     [&fd](const pollfd &p) { return p.fd == fd; }), fds.end());

            if (clientSocketsURI.count(fd)) {
                auto uri = clientSocketsURI.at(fd);
                auto *el = storage.getElement(uri);
                if (el) {
                    el->clearReader(fd);
                    if (el->getReadersCount() == 0)
                        storage.clearElement(uri);
                }
            }
            serverSocketsURI.erase(fd);
            clientSocketsURI.erase(fd);
            close(fd);
        }
    }

    pendingChanges.clear();
}
