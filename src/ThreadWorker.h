#pragma once


#include <thread>
#include <vector>
#include <sys/poll.h>
#include "cache/Storage.h"
#include "helpers/HttpParser.h"

enum class PipeOperation {
    Add,
    Remove
};

class ThreadWorker {
public:
    explicit ThreadWorker(Storage &storage);

    void storeClientConnection(int fd);

private:
    std::vector<pollfd> fds;
    std::thread thread;
    Storage &storage;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

    int addPipeFd[2]{};
    int removePipeFd[2]{};

    std::vector<std::pair<int, PipeOperation>> pendingChanges;

    void worker();

    std::map<int, std::string> serverSocketsURI;
    std::map<int, std::string> clientSocketsURI;
    std::map<int, std::string> clientBuffersMap;

    void handleClientConnection(pollfd &pfd);

    void handleReadDataFromServer(pollfd &pfd);


    void handleClientInput(pollfd &pfd);

    void handleClientReceivingResource(pollfd &pfd);

    void readClientInput(int fd);

    void addPipe(int writeEnd);

    void removePipe(int writeEnd);

    void handlePipeMessages();

    void applyPendingChanges();
};